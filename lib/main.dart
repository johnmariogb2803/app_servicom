import 'package:app_servicom/GUI01/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '_global/preferences.dart';
import 'login/main.dart';

void main() => runApp(MyApp());

// ignore: must_be_immutable
class MyApp extends StatelessWidget{
  Preferences prefs = Preferences();

  Future<void> initPrefs() async {
    prefs = await prefs.init();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: initPrefs(),
      builder: (context, snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          return MaterialApp(
            title: 'Login Demo',
            theme: ThemeData(
              primaryColor: Colors.blueGrey, // COLOR PRINCIPAL
              //canvasColor: Colors.lightBlue[200], // COLOR DE FONDO
              cardTheme: CardTheme(
                color: Colors.blueGrey,
              ),
              inputDecorationTheme: InputDecorationTheme(
                fillColor: Colors.grey[300],
                isCollapsed: true,
                hintStyle: TextStyle(
                  color: Colors.grey,
                ),
              ),
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.grey[300]!),
                  foregroundColor: MaterialStateProperty.all<Color>(Colors.blueGrey)
                )
              ),
              iconTheme: IconThemeData(
                color: Colors.blueGrey,
              ),
              textSelectionTheme: TextSelectionThemeData(
                cursorColor: Colors.grey,
                selectionHandleColor: Colors.grey,
                selectionColor: Colors.grey
              ),
            ),
            initialRoute: '/login',
            routes: {
              '/login': (context) => Login(),
              '/main_menu': (context) => MainGUI01(),
            },
            home: prefs.sesion! ? MainGUI01() : Login(),
            //home: Login(),
          );
        } else {
          return MaterialApp(
            home: Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
      },
    );
  }
}

