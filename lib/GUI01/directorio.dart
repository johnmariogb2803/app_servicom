import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GUIDirectorio extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GUIDirectorio_();
  }
}

class GUIDirectorio_ extends State<GUIDirectorio> {
  //ADICIONAR OPCIONES EN EL DICCIONARIO
  Map<int, String> listaDiretorios = {
    0: "Hoteles",
    1: "Bares",
    2: "Restaurantes",
    3: "Drograrias",
    4: "Centros comerciales",
    5: "Ferreterias"
  };

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsetsDirectional.all(10),
        child: Column(
          children: [
            Flexible(
                child: GridView.count(
                    // Create a grid with 2 columns. If you change the scrollDirection to
                    // horizontal, this produces 2 rows.
                    crossAxisCount: 4,
                    children: List.generate(
                      listaDiretorios.length,
                      (index) {
                        return TextButton(
                            onPressed: () {},
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.phone_in_talk_sharp),
                                Text(
                                  listaDiretorios.values.toList()[index],
                                  style: TextStyle(
                                      fontSize: 9,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                )
                              ],
                            ));
                      },
                    ))),
            Flexible(
              child: Container(
                child: Text("Aqui resultados de busqueda"),
              ),
            )
          ],
        ));
  }
}

void hacerAlgoParaCadaOpcion(tipoDelito) {
  switch (tipoDelito) {
    case 0:
      Fluttertoast.showToast(
          msg: "Hacer algo para la opcion $tipoDelito.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.grey,
          fontSize: 16.0);
      break;
    case 1:
      Fluttertoast.showToast(
          msg: "Hacer algo para la opcion $tipoDelito.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.grey,
          fontSize: 16.0);
      break;
  }
}
