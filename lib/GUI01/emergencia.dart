import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:fluttertoast/fluttertoast.dart';

class GUIEmergencia extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GUIEmergencia_();
  }
}

class GUIEmergencia_ extends State<GUIEmergencia> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
              onPressed: null,
              onLongPress: () {
                Fluttertoast.showToast(
                    msg: "Hacer lo que tenga que hacer.",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    fontSize: 16.0
                );
              },
              child: Image.asset(
                'assets/1.png',
                scale: 3,
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                  onPressed: () {},
                  child: Image.asset(
                    'assets/2.png',
                    scale: 3,
                  )),
              TextButton(
                  onPressed: () {},
                  child: Image.asset(
                    'assets/3.png',
                    scale: 3,
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
