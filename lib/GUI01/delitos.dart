import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GUIDelitos extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GUIDelitos_();
  }
}

class GUIDelitos_ extends State<GUIDelitos> {
  //ADICIONAR OPCIONES EN EL DICCIONARIO
  Map<int, dynamic> listaDelitos = {
    0: ["Robo", Icon(Icons.adjust)],
    1: ["Extorsión", Icon(Icons.admin_panel_settings)],
    2: ["Fuga presos", Icon(Icons.people)],
    3: ["Secuestro", Icon(Icons.accessibility_rounded)],
    4: ["Robo", Icon(Icons.adjust)],
    5: ["Extorsión", Icon(Icons.admin_panel_settings)],
    6: ["Fuga presos", Icon(Icons.people)],
    7: ["Secuestro", Icon(Icons.accessibility_rounded)]
  };

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsetsDirectional.all(10),
        child: GridView.count(
          // Create a grid with 2 columns. If you change the scrollDirection to
          // horizontal, this produces 2 rows.
          crossAxisCount: 4,
          // Generate 100 widgets that display their index in the List.
          children: List.generate(listaDelitos.length, (index) {
            //FUNCION QUE CONSTRUYE LAS OPCIONES REGISTRADAS EN EL DICCIONARIO
            return TextButton(
                onPressed: () {},
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    listaDelitos.values.toList()[index][1],
                    Text(
                      listaDelitos.values.toList()[index][0],
                      style:
                          TextStyle(fontSize: 9, fontWeight: FontWeight.bold, color: Colors.black),
                    )
                  ],
                ));
          }),
        )
        /*ListView(
        children: List.generate(listaDelitos.length, (index) {
          //FUNCION QUE CONSTRUYE LAS OPCIONES REGISTRADAS EN EL DICCIONARIO
          return Padding(
            padding: EdgeInsets.all(0),
            child: ListTile(
              leading: listaDelitos.values.toList()[index][1],
              title: Text(listaDelitos.values.toList()[index][0]),
              trailing: Icon(Icons.keyboard_arrow_down_outlined),
              onTap: () {
                // FUNCION QUE DISPARA EVENTO
                hacerAlgoParaCadaOpcion(listaDelitos.keys.toList()[index]);
              },
            ),
          );
        }),
      ),*/
        );
  }
}

void hacerAlgoParaCadaOpcion(tipoDelito) {
  switch (tipoDelito) {
    case 0:
      Fluttertoast.showToast(
          msg: "Hacer algo para la opcion $tipoDelito.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.grey,
          fontSize: 16.0);
      break;
    case 1:
      Fluttertoast.showToast(
          msg: "Hacer algo para la opcion $tipoDelito.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.grey,
          fontSize: 16.0);
      break;
    case 2:
      Fluttertoast.showToast(
          msg: "Hacer algo para la opcion $tipoDelito.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.grey,
          fontSize: 16.0);
      break;
  }
}
