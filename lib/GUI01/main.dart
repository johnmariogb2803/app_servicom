import 'package:app_servicom/GUI01/emergencia.dart';
import 'package:flutter/material.dart';

import 'delitos.dart';
import 'directorio.dart';

class MainGUI01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: "Emergencia", icon: Icon(Icons.announcement_outlined)),
              Tab(text: "Delitos", icon: Icon(Icons.admin_panel_settings)),
              Tab(text: "Directorio", icon: Icon(Icons.phone_in_talk_sharp)),
            ],
          ),
          title: Row(
            children: [
              Hero(
                  tag: "logo",
                  child: Icon(
                    Icons.security,
                    size: 20,
                  )),
              SizedBox(width: 10,),
              Text('Servicom')
            ],
          ),
        ),
        body: TabBarView(
          children: [
            GUIEmergencia(),
            GUIDelitos(),
            GUIDirectorio(),
          ],
        ),
      ),
    );
  }
}
