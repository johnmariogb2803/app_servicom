import 'package:app_servicom/_global/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Flexible(
              child: Center(
            child: Hero(
                tag: "logo",
                child: Icon(
                  Icons.security,
                  size: 80,
                )),
          )),
          Card(
            elevation: 60,
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 60.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: LoginForm(),
            ),
          ),
        ],
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginForm createState() {
    return _LoginForm();
  }
}

class _LoginForm extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController user = TextEditingController();
  TextEditingController passwd = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              controller: user,
              decoration: InputDecoration(
                filled: true,
                hintText: 'Usuario',
                prefixIcon: Icon(Icons.person, color: Colors.grey),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                contentPadding: EdgeInsets.all(20),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Falta ingresar el usuario';
                }
              },
            ),
            SizedBox(height: 20.0),
            TextFormField(
              obscureText: true,
              controller: passwd,
              decoration: InputDecoration(
                filled: true,
                hintText: 'Contraseña',
                prefixIcon: Icon(Icons.lock, color: Colors.grey),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                contentPadding: EdgeInsets.all(20),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Falta ingresar la contraseña';
                }
              },
            ),
            SizedBox(height: 30.0),
            loading
                ? Center(
                    child: CircularProgressIndicator(color: Colors.grey,),
                  )
                : Container(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            FocusScope.of(context).requestFocus(FocusNode());
                            setState(() {
                              loading = true;
                            });
                            //if (await user_auth(user.text, passwd.text)) {
                            if (true) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  '/main_menu',
                                  (Route<dynamic> route) => false);
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Usuario o contraseña incorrectos. Por favor intente nuevamente.",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.TOP,
                                  timeInSecForIosWeb: 2,
                                  fontSize: 16.0);
                            }
                            setState(() {
                              loading = false;
                            });
                          }
                        },
                        child: Text("INGRESAR")),
                  )
          ],
        ));
  }
}
