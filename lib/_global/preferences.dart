import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static const PREFIX = "App001";
  static const SESION = "$PREFIX SESION";
  static const ID_USUARIO = "$PREFIX ID_USUARIO";
  static const NOMBRE = "$PREFIX NOMBRE";
  static const USUARIO = "$PREFIX USUARIO";

  static Preferences get insance => Preferences._internal();
  SharedPreferences? prefs;

  bool? sesion = false;
  String? id_usuario, nombre, usuario;

  Preferences._internal();

  factory Preferences() => insance;

  Future<SharedPreferences> get preferences async {
    if (prefs != null) {
      return prefs!;
    } else {
      prefs = await SharedPreferences.getInstance();
      sesion = prefs!.getBool(SESION);
      id_usuario = prefs!.getString(ID_USUARIO);
      nombre = prefs!.getString(NOMBRE);
      usuario = prefs!.getString(USUARIO);
    }

    return prefs!;
  }

  Future<void> commit() async {
    await prefs!.setBool(SESION, sesion!);
    await prefs!.setString(ID_USUARIO, id_usuario!);
    await prefs!.setString(NOMBRE, nombre!);
    await prefs!.setString(USUARIO, usuario!);
  }

  Future<void> clear() async {
    sesion = false;
    id_usuario = null;
    nombre = null;
    usuario = null;
    await this.commit();
  }

  Future<Preferences> init() async {
    prefs = await preferences;
    return this;
  }
}
