import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:app_servicom/_global/preferences.dart';

var url = "http://192.168.0.14:9000/";

Future<bool> user_auth(user, pass) async {
  var response = await http.post(Uri.parse(url + "login.php"), body: {
    "usuario": user,
    "contrasena": pass
  });

  if (response.statusCode == 200) {
    try{
      var data = json.decode(response.body);
      print("Valid user:" + data[0].toString());
      Preferences prefs = Preferences();
      prefs = await prefs.init();

      prefs.sesion = true;
      prefs.id_usuario = data[0]["idUsuario"];
      prefs.nombre = data[0]["nombre"];
      prefs.usuario = data[0]["usuario"];
      prefs.commit();

      return true;
    }catch (e){
      print(e);
      print(response.body);
      return false;
    }
  } else {
    print('Request failed with status: ${response.statusCode}.');
    return false;
  }
}


